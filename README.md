snorelo
=======
Snore detection camera to record my snoring. 

![Example video](static/snorelo_output_2021-04-14_032636.mp4)

<!-- <figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="static/snorelo-poster.jpg">
    <source src="static/snorelo_output_2021-04-14_032636.mp4" type="video/mp4">
  </video>
</figure> -->


Lots of help from:
* Video: https://www.pyimagesearch.com/2015/05/25/basic-motion-detection-and-tracking-with-python-and-opencv/
* Audio: https://stackoverflow.com/questions/14140495/how-to-capture-a-video-and-audio-in-python-from-a-camera-or-webcam

Setup
-----
0. As of Aug 2021, I'm able to allow VSCode to access my camera and mic. See the most current `launch.json` in the faces branch.

    Previously, I had to follow this link to get my iTerm to be able to access the camera (and will probably have to do something similar to allow it to access the microphone) https://apple.stackexchange.com/questions/360851/add-access-to-the-macbook-camera-for-the-terminal-application

1. Install python 3.8 for OpenCV and PyAudio

        conda create -n snorelo python=3.8
        conda activate snorelo
        conda install -c conda-forge opencv
        conda install -c anaconda pyaudio
        pip install imutils

2. Copy the Haar Feature-based Cascade Classifier into `models/`

    cd models
    wget https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_frontalface_default.xml


Usage
-----
Execute the script, output is in `output/` and logs are in `logs/`. Look for the stdout logs keeps track of all of the multithreading

    ./run_snorelo.sh

* Toggle the `c`olor between RGB and gray with `c`
* Quit the program by clicking on the video window and typing `q` 

Epilogue
--------
My insurance covered most of this and my wife is happier
https://glidewelldental.com/solutions/sleep-dentistry/mandibular-advancement-devices/silent-nite-sleep-appliance