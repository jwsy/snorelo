import audioop
import cv2
import datetime
import imutils
import json
import logging
from multiprocessing import Process
import os
import pyaudio
import shlex
import subprocess
import time
import wave

# Set input params and camera type: https://apple.stackexchange.com/questions/360851/add-access-to-the-macbook-camera-for-the-terminal-application
CLIP_LENGTH_SEC = 20
CAMERA_TYPE = {
    'SNAP': 0,
    'BUILTIN': 1 # frame.shape = (720, 1280, 3)
}
VIDEO_FRAME_RATE = 15.0
OUTPUT_FRAME_SHAPE = (500, 281)
VOLUME_THRESHOLD_RMS = 400
VOLUME_THRESHOLD_RMS_COOLDOWN_SEC = 3
CHUNK_SIZE = 1024
CHANNELS = 1
FORMAT = pyaudio.paInt16
RATE = 44100
CASCADE_SCALEFACTOR = 2
CASCADE_MINNEIGHBORS = 5

def process_raw(input_video_file, input_audio_file, filename_dt):
    dt = datetime.datetime.now()

    # Determine durations of video and audio
    print(f"""{dt} process_raw(), pid: {os.getpid()}, video: {input_video_file}, audio: {input_audio_file}""")
    ffprobe_v_cmd = f"ffprobe -v quiet -print_format json -show_format {input_video_file}"
    fvcp = subprocess.run(shlex.split(ffprobe_v_cmd), capture_output=True)
    fvcp_j = json.loads(fvcp.stdout)
    input_video_duration = fvcp_j['format']['duration']

    ffprobe_a_cmd = f"ffprobe -v quiet -print_format json -show_format {input_audio_file}"
    facp = subprocess.run(shlex.split(ffprobe_a_cmd), capture_output=True)
    facp_j = json.loads(facp.stdout)
    input_audio_duration = facp_j['format']['duration']

    print(f"{dt} input_video_duration: {input_video_duration}s; input_audio_duration: {input_audio_duration}s")

    # Correct media speed using setpts and atempo
    v_pts = 1.0 * CLIP_LENGTH_SEC / float(input_video_duration)
    ffmpeg_v_speed_out_filename = f"output/corrected_speedv_{filename_dt.strftime('%Y-%m-%d_%H%M%S')}.mp4"
    ffmpeg_v_speed_cmd = f"""ffmpeg -loglevel quiet -i {input_video_file} -r 15 -filter:v "setpts={v_pts}*PTS" {ffmpeg_v_speed_out_filename}"""
    print(f"{dt} running: {ffmpeg_v_speed_cmd}")
    ffmpeg_vsc_subprocess = subprocess.run(shlex.split(ffmpeg_v_speed_cmd), capture_output=True)
    print(f"{dt} ffmpeg_vsc_subprocess.stderr: {ffmpeg_vsc_subprocess.stderr[:100]}")
    print(f"{dt} [+] Created correct-speed video file {ffmpeg_v_speed_out_filename}")

    a_atempo = float(input_audio_duration) / CLIP_LENGTH_SEC
    ffmpeg_a_speed_out_filename = f"output/corrected_speeda_{filename_dt.strftime('%Y-%m-%d_%H%M%S')}.wav"
    # ffmpeg_a_speed_cmd = f"""ffmpeg -loglevel quiet -i {input_audio_file} -filter:a "highpass=f=200, lowpass=f=3000, atempo={a_atempo}" -vn {ffmpeg_a_speed_out_filename}"""
    ffmpeg_a_speed_cmd = f"""ffmpeg -loglevel quiet -i {input_audio_file} -filter:a "atempo={a_atempo}" -vn {ffmpeg_a_speed_out_filename}"""
    print(f"{dt} running: {ffmpeg_a_speed_cmd}")
    ffmpeg_asc_subprocess = subprocess.run(shlex.split(ffmpeg_a_speed_cmd), capture_output=True)
    print(f"{dt} ffmpeg_asc_subprocess.stderr: {ffmpeg_asc_subprocess.stderr[:100]}")
    print(f"{dt} [+] Created correct-speed audio file {ffmpeg_a_speed_out_filename}")

    # Combine newly created media
    combined_output_file = f"output/snorelo_output_{dt.strftime('%Y-%m-%d_%H%M%S')}.mp4"
    # combine_aud_vid_cmd = f"ffmpeg -loglevel quiet -ac 2 -channel_layout stereo -i {ffmpeg_v_speed_out_filename} -i {ffmpeg_a_speed_out_filename} -pix_fmt yuv420p {combined_output_file}"
    combine_aud_vid_cmd = f"ffmpeg -loglevel quiet -ac 2 -channel_layout stereo -i {ffmpeg_v_speed_out_filename} -i {ffmpeg_a_speed_out_filename} -pix_fmt yuv420p {combined_output_file}"
    combine_aud_vid = subprocess.run(shlex.split(combine_aud_vid_cmd))
    print(f"{dt} [+++++] Created combined file {combined_output_file}")

    # Clean up temp corrected files
    # if os.path.exists(ffmpeg_v_speed_out_filename) and os.path.exists(ffmpeg_a_speed_out_filename):
        # print(f"{dt} [-] Deleting {ffmpeg_v_speed_out_filename} and {ffmpeg_a_speed_out_filename}")
        # os.remove(ffmpeg_v_speed_out_filename)
        # os.remove(ffmpeg_a_speed_out_filename)

    print(f"{dt} pid {os.getpid()} complete")
    

if __name__ == '__main__':
    logname = "log/snorelo_log_" + time.strftime("%Y-%m-%d_%H%M") + ".log"
    logging.basicConfig(filename=logname, level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    # https://towardsdatascience.com/face-detection-in-2-minutes-using-opencv-python-90f89d7c0f81
    face_cascade = cv2.CascadeClassifier('models/haarcascade_frontalface_default.xml')
    audio = pyaudio.PyAudio()

    # Initialize output files
    volume_threshold_hit_last_dt = datetime.datetime.now()
    last_dt = datetime.datetime.now()
    out_filename = f"output/snorelo_raw_output_video-{last_dt.strftime('%Y-%m-%d_%H%M%S')}.mp4"
    out_vid = cv2.VideoWriter(out_filename, fourcc, fps=VIDEO_FRAME_RATE, frameSize=OUTPUT_FRAME_SHAPE)
    outa_filename = f"output/snorelo_raw_output_audio-{last_dt.strftime('%Y-%m-%d_%H%M%S')}.wav"
    audio_frames = []
    frame_count = 0

    # Read from audio & camera
    audio_stream = audio.open(format=FORMAT, channels=CHANNELS,
                                rate=RATE, input=True,
                                frames_per_buffer=CHUNK_SIZE)
    video_stream = cv2.VideoCapture(CAMERA_TYPE['BUILTIN'])
    if not video_stream.isOpened():
        logging.error('Cannot open camera')
        exit(1)
    time.sleep(2.0)
    # video_stream = cv2.VideoCapture(args["video"])

    is_grayscale = False
    is_blur = False
    is_recording = True
    init_ignore_recording = False # refactor and set to true later
    detected_volume_above_threshold = False

    # loop over the frames of the video
    while True:
        # Grab the current frame, if the frame could not be grabbed, then we have reached the end of the video
        curr_dt = datetime.datetime.now()
        ret, frame = video_stream.read()
        if frame is None:
            break
        if frame_count == 0:
            audio_frames = []
        frame_count += 1

        audio_data = audio_stream.read(CHUNK_SIZE, exception_on_overflow = False) 
        audio_frames.append(audio_data)
        rms = audioop.rms(audio_data,2) # this is the "volume"

        # Initialize the info text 
        rec_status_text = '***RECORDING***' if is_recording else 'Not recording'
        color_filter_text = 'RGB' if not is_grayscale else 'Grayscale'

        # Update the file if CLIP_LENGTH_SEC have elapsed since last datetime
        time_diff = curr_dt - last_dt
        if time_diff.seconds >= CLIP_LENGTH_SEC:
            # Write the video file
            print(f"Closing {out_filename} after {frame_count} frames {frame_count/CLIP_LENGTH_SEC} fps, updated {last_dt.strftime('%Y-%m-%d_%H%M%S')}")
            logging.debug(f"Closing {out_filename} after {frame_count} frames {frame_count/CLIP_LENGTH_SEC} fps, updated {last_dt.strftime('%Y-%m-%d_%H%M%S')}")
            out_vid.release()

            # Write the audio file
            logging.debug(f"Closing {outa_filename} after {len(audio_frames)/CLIP_LENGTH_SEC} fps, updated {last_dt.strftime('%Y-%m-%d_%H%M%S')}")
            out_aud = wave.open(outa_filename, 'wb')
            out_aud.setnchannels(CHANNELS)
            out_aud.setsampwidth(audio.get_sample_size(FORMAT))
            out_aud.setframerate(RATE)
            out_aud.writeframes(b''.join(audio_frames))
            out_aud.close()

            # Combine the files in another thread
            if init_ignore_recording == True or detected_volume_above_threshold == True:
                p = Process(target=process_raw, args=(out_filename, outa_filename, last_dt))
                p.start()

            # Re-initialize output files, delete if no sound
            if os.path.exists(out_filename) and os.path.exists(outa_filename) and detected_volume_above_threshold == False:
                logging.debug(f"[-] Deleting {out_filename} and {outa_filename}")
                os.remove(out_filename)
                os.remove(outa_filename)

            detected_volume_above_threshold = False
            frame_count = 0
            last_dt = curr_dt
            out_filename = f"output/snorelo_raw_output_video-{last_dt.strftime('%Y-%m-%d_%H%M%S')}.mp4"
            out_vid = cv2.VideoWriter(out_filename, fourcc, fps=VIDEO_FRAME_RATE, frameSize=OUTPUT_FRAME_SHAPE)
            outa_filename = f"output/snorelo_raw_output_audio-{last_dt.strftime('%Y-%m-%d_%H%M%S')}.wav"
            audio_frames = []


        # resize the frame, convert it to grayscale, and blur it # frame.shape = (281, 500, 3)
        frame = imutils.resize(frame, width=500) 
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # gray = cv2.GaussianBlur(gray, (21, 21), 0)
        faces = face_cascade.detectMultiScale(gray, CASCADE_SCALEFACTOR, CASCADE_MINNEIGHBORS)
        # Draw the rectangle around each face
        for (x, y, w, h) in faces:
            cv2.rectangle(gray, (x, y), (x+w, y+h), (255, 0, 0), 2)
            sub_face = gray[y:y+h, x:x+w]
            sub_face = cv2.GaussianBlur(sub_face,(23, 23), 30)
            gray[y:y+sub_face.shape[0], x:x+sub_face.shape[1]] = sub_face

            cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)

        if is_grayscale:
            frame = gray
        
        # Draw text
        if rms > VOLUME_THRESHOLD_RMS:
            detected_volume_above_threshold = True
        if rms > VOLUME_THRESHOLD_RMS and (curr_dt - volume_threshold_hit_last_dt).seconds >= VOLUME_THRESHOLD_RMS_COOLDOWN_SEC:
            print(f"""{curr_dt} [+] Detected volume above {VOLUME_THRESHOLD_RMS} ({rms}) at {curr_dt}, last high volume detected {((curr_dt - volume_threshold_hit_last_dt).seconds)}s ago""")
            logging.info(f"[+] Detected volume above {VOLUME_THRESHOLD_RMS} ({rms}) at {curr_dt}, last high volume detected {((curr_dt - volume_threshold_hit_last_dt).seconds)}s ago")
            volume_threshold_hit_last_dt = curr_dt
        # putText_color = (255 - 255 * (1.0-1.0/rms), 255, 155 - 155 * (1.0-1.0/rms))
        putText_color = (255, rms, 255 - 255 * (1.0-1.0/(0.001 + rms)))
        cv2.putText(frame, f"Snorelo cam {rec_status_text} - {color_filter_text} - {rms}", (10, 40),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, putText_color, 2)
        cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                    (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, putText_color, 1)
        
        # Finally draw window
        cv2.imshow('Snorecam', frame)

        # Record frames
        if is_recording:
            out_vid.write(frame)
            audio_frames.append(audio_data)

        # Get interaction
        key = cv2.waitKey(1) & 0xFF
        # if key == ord('r'):
        #     is_recording = not is_recording
        if key == ord('c'):
            is_grayscale = not is_grayscale
        if key == ord('b'):
            is_blur = not is_blur
        if key == ord('q'):
            break

    # Upon exit
    video_stream.release()
    out_vid.release()
    cv2.destroyAllWindows()
