#!/bin/bash +x 


# conda activate snorelo
eval "$(conda shell.bash hook)"
conda activate snorelo

LOGFILE=snorelo_log_stdout_$(date +"%Y-%m-%d_%H%M%S").log

python snorelo.py | tee log/$LOGFILE
